package Models;

import sun.java2d.pipe.SpanShapeRenderer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public  class Functions {

    public void searchActorFilmography(String name, String surname) {
        ArrayList<String> movies = new ArrayList<>();

        for (Movie movie : Library.movies) {
            for (Actor actor : movie.getListOfActors()) {
                if (actor.getFirstName().equals(name) & (actor.getLastName().equals(surname))) {
                    movies.add(movie.getTitle());
                }
            }
        }

        if (movies.size() == 0) {
            System.out.println(name + " " + surname + " didn't play in any movie from library");
        } else {
            System.out.println("Actor " + name + " " + surname + " have played in movies: ");
            String separator = "";

            for (String movie : movies) {
                System.out.print(separator + movie);
                separator = ", ";
            }
        }
    }

    public void searchForMovie(String title) {


        if (searchingTitle(title) == null) {
            System.out.println("MovieLibrary " + title + " isn't saved in library");
        } else {
            System.out.println("MovieLibrary " + searchingTitle(title).getTitle() + " found in library");
        }

    }

    public void printMovieInformations(String title) {
        if (searchingTitle(title) == null) {
            System.out.println("MovieLibrary " + title + " isn't saved in library");
        } else {

            Movie movie = searchingTitle(title);
            printMovieInformation(movie);
        }
    }

    public void printAllMoviesInformations() {
        if (Library.movies.size() == 0) {
            System.out.println("There is no movies in library");
        } else {
            for (Movie movie : Library.movies) {
                printMovieInformation(movie);
                System.out.println("\n");
            }
        }
    }

    public void searchMoviesInDateRange(Date startDate, Date endDate) {
        ArrayList<String> movies = new ArrayList<>();

        for (Movie movie : Library.movies) {
            if (((movie.getDate().equals(startDate) || movie.getDate().after(startDate)) &&
                    movie.getDate().equals(endDate) || movie.getDate().before(endDate))) {
                movies.add(movie.getTitle());
            }
        }
        if (movies.size() != 0) {
            System.out.println("Found " + movies.size() + " movies in given date range: ");
            for (String title : movies) {
                System.out.println(title);
            }
        } else {
            System.out.println("There is no movies in given date range");
        }
    }

    private Movie searchingTitle(String searchingTitle) {
        for (Movie movie : Library.movies) {
            if (movie.getTitle().contains(searchingTitle)) {
                return movie;
            }
        }
        return null;
    }



    private void printMovieInformation(Movie movie) {

        System.out.println("Title: " + movie.getTitle());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        System.out.println("Production date: " + formatter.format(movie.getDate()));
        System.out.println("Genre: " + movie.getGenre());
        System.out.println("Director: " + movie.getDirector().getFirstName() + " " + movie.getDirector().getLastName());
        System.out.print("Actors:");
        String separator = " ";

        for (Person actor : movie.getListOfActors()) {
            System.out.print(separator + actor.getFirstName() + " " + actor.getLastName());
            separator = ", ";
        }
    }


}
