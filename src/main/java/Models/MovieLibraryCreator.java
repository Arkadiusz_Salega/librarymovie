package Models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;


public class MovieLibraryCreator {

    private static Scanner scanner = new Scanner(System.in);
    private static Functions functions = new Functions();


    public static void startMovieLibrary() {
        System.out.println("Running MovieLibrary");
    }

    public static void printMenu() {
        System.out.println("\nAvailable actions: \npress");
        System.out.println("0 - to shutdown\n" +
                "1 - to print movies in library\n" +
                "2 - to print all information about selected movieLibrary\n" +
                "3 - to search movieLibrary by a title/part of title\n" +
                "4 - to show movies when selected actor have played\n" +
                "5 - to show all movies within date range\n" +
                "6 - to read movies from JSON file\n" +
                "7 - to write movies to JSON file\n" +
                "8 - to read movies from XML file\n" +
                "9 - to write movies to XML file\n" +
                "10 - to write movies to Excel file\n" +
                "11 - to print a list of available actions.");
        System.out.println("Choose your action:     ");
    }

    public enum Acts {
        QUIT(0), PRINTALLMOVIES(1), PRINTONEMOVIEINFO(2), SEARCHBYTITLE(3), ACTORFILMOGRAPHY(4), MOVIESINDATERANGE(5), JSONREAD(6), JSONWRITE(7),XMLREAD(8),XMLWRITE(9),EXCELWRITE(10),ALLACTIONS(11);

        int action;

        Acts(int action) {
            this.action = action;
        }

        public int getAction() {
            return action;
        }

        public static Acts getStatusFromInt(int status) {
            for (Acts act : Acts.values()) {
                if (act.getAction() == status) {
                    return act;
                }
            }
            return null;
        }

    }

    public static int getActionNumber() {

        int number = scanner.nextInt();
        scanner.nextLine();
        return number;
    }

    public static void printMovieInformation() {
        System.out.println("Enter movieLibrary which are you interested in: ");
        functions.printMovieInformations(scanner.nextLine());

    }

    public static void printMovies() {

        functions.printAllMoviesInformations();
    }

    public static void searchMovie() {
        System.out.println("Enter a full/part of movieLibrary title to search: ");
        functions.searchForMovie(scanner.nextLine());
    }

    public static void moviesWithActor() {
        System.out.println("Enter a actor whose filmography you want to see: ");
        String[] actors = scanner.nextLine().split(" ");
        functions.searchActorFilmography(actors[0], actors[1]);
    }

    public static void moviesInRange()  {
        System.out.println("Enter a date range for searching movies. Correct date format is YYYY-MM-DD.");
        System.out.println("Separate two dates with \" \". ");
        String[] dates = scanner.nextLine().split(" ");

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        try {
            functions.searchMoviesInDateRange(formatter.parse(dates[0]), formatter.parse(dates[1]));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


} // END OF MovieLibraryCreator