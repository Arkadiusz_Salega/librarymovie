package Models;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;



public class JsonFunctions {

    public static void JsonDeserialization() {

        try {
            File file = new File("src\\main\\resources\\Movies.json");

            ObjectMapper objectMapper = new ObjectMapper();

            Library library = new Library(objectMapper.readValue(file, Movie[].class));
            System.out.println("JSON file read correctly");
        }catch (IOException e){
            e.printStackTrace();
            System.out.println("Some exception appears");
        }
}

public static void JsonSerialization(){
    ObjectMapper objectMapper = new ObjectMapper();

    try {
        objectMapper.writeValue(
                new FileOutputStream("WriteFolder/movies.json"), Library.movies);
        System.out.println("Movies.json added to WriteFolder");
    } catch (FileNotFoundException e) {
        e.printStackTrace();
        System.out.println("Some exception appear");

    }
    catch(IOException e){
        e.printStackTrace();
        System.out.println("Some exception appear");
    }

}
}
