package Models;

import java.util.ArrayList;
import java.util.Arrays;

public class Library {
    static ArrayList<Movie> movies = new ArrayList<>();

    public Library(Movie[] movies) {
        this.movies = new ArrayList<>(Arrays.asList(movies));
    }


    public ArrayList<Movie> getMovies() {
        return movies;
    }



}
