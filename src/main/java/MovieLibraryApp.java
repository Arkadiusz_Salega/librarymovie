import Models.*;

import static Models.MovieLibraryCreator.*;

public class MovieLibraryApp {
    public static void main(String[] args)  {

            boolean quit = false;
            startMovieLibrary();
            printMenu();
            while (!quit) {
                System.out.println("\nEnter action: (7 to show available actions)");


                switch (MovieLibraryCreator.Acts.getStatusFromInt(getActionNumber())) {

                    case QUIT:
                        System.out.println("\nQuiting...");
                        System.exit(0);
                        break;


                    case PRINTALLMOVIES:
                        printMovies();
                        break;

                    case PRINTONEMOVIEINFO:
                        printMovieInformation();
                        break;

                    case SEARCHBYTITLE:
                        searchMovie();
                        break;

                    case ACTORFILMOGRAPHY:
                        moviesWithActor();
                        break;

                    case MOVIESINDATERANGE:
                        moviesInRange();
                        break;

                    case JSONREAD:
                        JsonFunctions.JsonDeserialization();
                        break;

                    case JSONWRITE:
                        JsonFunctions.JsonSerialization();
                        break;

                    case XMLREAD:
                        XMLFunctions.XMLDeserialization();
                        break;

                    case XMLWRITE:
                        XMLFunctions.XMLDeserialization();
                        break;

                    case EXCELWRITE:
                        ExcelFunctions.ExcelSerialization();
                        break;

                    case ALLACTIONS:
                        printMenu();
                        break;
                }
            }
        }


}
